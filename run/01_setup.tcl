set outputDir ./out
set srcDir ../src/vhdl
set name Proj
set topName ${name}Top
file mkdir $outputDir

# Setup the design sources
read_vhdl $srcDir/$topName.vhdl
add_files $srcDir/vendor/
add_files $srcDir/vendor/uart/
add_files $srcDir/$name/

read_xdc $srcDir/constraints/${name}_Nexys4.xdc
