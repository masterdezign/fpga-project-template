module DataFlow
  where

import CLaSH.Prelude

-- | Composition of two not lifted dataflows
(>~>) f g a1 a2 a3 = (g1, g2, f3)
  where
    (g1, g2, g3) = g f1 f2 a3
    (f1, f2, f3) = f a1 a2 g3

(>=>) = seqDF
