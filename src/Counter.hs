module Counter
  where

import CLaSH.Prelude
import CLaSH.Prelude.Explicit

saturatingCounterWithResetT
  :: Integral a
  => a
  -> (a, Bool)
  -> (Bool, Bool)
  -> ((a, Bool), (a, Bool))
saturatingCounterWithResetT maxVal (cnt, stopped) (en, rst) = ((cnt', stopped'), (cnt, stopped))
  where
    cnt' | rst = 0
         | en && (not stopped) = cnt + 1
         | otherwise = cnt
    stopped' | rst = False
             | en && (cnt == maxVal - 1) = True
             | otherwise = stopped

-- | Counter stops as soon as maxVal is reached.
--   It reports if it's stopped.
--   The value is incremented only when it's not stopped and
--   input `en` is True.
saturatingCounterWithReset' :: Integral a =>
  SClock clk
  -> a
  -> (Signal' clk Bool, Signal' clk Bool)
  -> (Signal' clk a, Signal' clk Bool)
saturatingCounterWithReset' clk maxVal = mealyB' clk
  (saturatingCounterWithResetT maxVal) (0, False)

saturatingCounterWithReset :: Integral a =>
  a
  -> (Signal Bool, Signal Bool)
  -> (Signal a, Signal Bool)
saturatingCounterWithReset = saturatingCounterWithReset' sclock

-- | Counter reports only its status
-- maxVal up to 1
saturatingCounterWithReset1 ::
                               (Signal Bool, Signal Bool)
                            -> Signal Bool
saturatingCounterWithReset1 x = stopped
  where (_, stopped) = saturatingCounterWithReset' sclock 1 x

saturatingCounterWithReset4a :: (Unsigned 4)
                            -> (Signal Bool, Signal Bool)
                            -> (Signal (Unsigned 4), Signal Bool)
saturatingCounterWithReset4a maxVal x = saturatingCounterWithReset' sclock maxVal x

-- A stopped counter
saturatingCounterWithReset'' :: Integral a =>
  SClock clk
  -> a
  -> (Signal' clk Bool, Signal' clk Bool)
  -> (Signal' clk a, Signal' clk Bool)
saturatingCounterWithReset'' clk maxVal = mealyB' clk
  (saturatingCounterWithResetT maxVal) (maxVal, True)

-- Initialized as a stopped 6bit saturating counter
saturatingCounterWithReset6b :: (Unsigned 6)
                            -> (Signal Bool, Signal Bool)
                            -> (Signal (Unsigned 6), Signal Bool)
saturatingCounterWithReset6b maxVal x = saturatingCounterWithReset'' sclock maxVal x

-- Initialized as a stopped counter
saturatingCounterWithReset1b ::
                               (Signal Bool, Signal Bool)
                            -> Signal Bool
saturatingCounterWithReset1b x = stopped
  where (_, stopped) = saturatingCounterWithReset'' sclock 1 x

-- | Counter reports only its status
-- maxVal up to (2^14 - 1)
saturatingCounterWithReset14 :: (Unsigned 14)
                            -> (Signal Bool, Signal Bool)
                            -> Signal Bool
saturatingCounterWithReset14 maxVal x = stopped
  where (_, stopped) = saturatingCounterWithReset' sclock maxVal x
        --
-- | Counter reports only its status.
-- maxVal up to (2^26 - 1)
saturatingCounterWithReset26 :: (Unsigned 26)
                            -> (Signal Bool, Signal Bool)
                            -> Signal Bool
saturatingCounterWithReset26 maxVal x = stopped
  where (_, stopped) = saturatingCounterWithReset' sclock maxVal x

skipCounterT maxVal skip (ocnt, scnt) en = ((ocnt', scnt'), ocnt)
  where scnt' | rstScnt = 0
              | en = scnt + 1
              | otherwise = scnt

        ocnt' | rstScnt && maxVal == ocnt = 0
              | rstScnt = ocnt + 1
              | otherwise = ocnt

        rstScnt = en && scnt == skip

-- | This counter works on enable signal `en`
-- and increases only after certain skip period
skipCounter :: Unsigned 9 -> Unsigned 9
            -> Signal Bool -> Signal (Unsigned 9)
skipCounter maxVal skip = mealyB (skipCounterT maxVal skip) (0, 0)

-- | Just a cyclic counter which increases on `en` signal
counterEnT :: (Eq t, Num t) => t -> t -> Bool -> (t, t)
counterEnT maxVal cnt en = (cnt', cnt)
  where cnt' | en && maxVal - 1 == cnt = 0
             | en = cnt + 1
             | otherwise = cnt

counterEn :: Unsigned 9 -> Signal Bool -> Signal (Unsigned 9)
counterEn = counterEn' sclock

counterEn15 :: Unsigned 15 -> Signal Bool -> Signal (Unsigned 15)
counterEn15 = counterEn' sclock

counterEn16 :: Unsigned 16 -> Signal Bool -> Signal (Unsigned 16)
counterEn16 = counterEn' sclock

counterEn'
  :: (Eq a, Num a, Bundle a) =>
       SClock clk -> a -> Signal' clk Bool -> Unbundled' clk a
counterEn' clk maxVal = mealyB' clk (counterEnT maxVal) 0
