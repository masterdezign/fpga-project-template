module Hardware.Uart
  where

import CLaSH.Prelude

import DataFlow ( (>~>) )

-- https://hackage.haskell.org/package/clash-prelude/docs/CLaSH-Prelude-DataFlow.html
-- A circuit
-- adhering to the 'DataFlow' type should:
--
--  * Not consume data when validity is deasserted.
--  * Only update its output when readiness is asserted.

-- UART to data flow adapter
uart2df :: Signal (BitVector 8) -> Signal Bit -> Signal Bool -> (Signal (BitVector 8), Signal Bool, Signal Bit)
uart2df iData iStrobe oReady = (oData, oValid, iAck)
  where
    (oData, oValid, iAck') = (df $ fifoDF d2 Nil) iData (fmap (high==) iStrobe) oReady
    iAck = mux iAck' (signal high) (signal low)

df2uartT :: (Bit, Bool) -> (t, Bool, Bit) -> ((Bit, Bool), (t, Bit, Bool))
df2uartT (oStrobe, iReady) (iData, iValid, oAck) = ((oStrobe', iReady'), (iData, oStrobe', iReady'))
  where
    -- oStrobe goes low as soon as receiver has acknowledged
    oStrobe' | oAckB = low
             | iValid = high
             | otherwise = oStrobe

    iReady' | oAckB = True
            | iValid || (oStrobe == high) = False
            | otherwise = iReady

    oAckB = oAck == high

-- Data flow to UART adapter
df2uart :: Signal (BitVector 8) -> Signal Bool -> Signal Bit -> (Signal (BitVector 8), Signal Bit, Signal Bool)
df2uart a b c = f (a, b, c)
  where f = df2uartT `mealyB` (low, True)

loopback
  :: Signal (BitVector 8)
     -> Signal Bit
     -> Signal Bit
     -> (Signal (BitVector 8), Signal Bit, Signal Bit)
loopback = uart2df >~> df2uart
