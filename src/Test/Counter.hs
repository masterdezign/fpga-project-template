module Test.Counter
  where

import CLaSH.Prelude
import CLaSH.Prelude.Explicit

import Counter

-- Data for saturating counter

testInput5 :: Signal (Bool, Bool)
testInput5 = stimuliGenerator $(v [(True, False)::(Bool, Bool), (True, False), (True, False)
                                            , (True, False), (True, False), (True, False)
                                            , (True, False), (True, False), (True, True)
                                            , (True, False), (True, False), (True, False)
                                            , (True, False), (True, False), (True, True)
                                            , (True, False), (True, False), (True, False)
                                            , (True, False), (True, False), (True, False) ])

expectedOutput5 = outputVerifier $(v [ (0, False)::(Unsigned 9, Bool), (1, False), (2, False)
                                               , (3, True), (3, True), (3, True)
                                               , (3, True), (3, True), (3, True)
                                               , (0, False), (1, False), (2, False)
                                               , (3, True), (3, True), (3, True)
                                               , (0, False), (1, False), (2, False)
                                               , (3, True), (3, True), (3, True) ])

-- Skip counter data

testInput8 :: Signal Bool
testInput8 = stimuliGenerator $(v [ True :: Bool, False, True, False
                                     , True, False, True, False
                                     , True, False, True, False
                                     , True, False, True, False
                                     , True, False, True, False
                                     , True, False, True, False
                                     , True, False, True, False
                                   ])

expectedOutput8 :: Signal (Unsigned 9) -> Signal Bool
expectedOutput8 = outputVerifier $(v [ 0 :: (Unsigned 9), 0, 0, 1
                                      , 1, 1, 1, 2
                                      , 2, 2, 2, 0
                                      , 0, 0, 0, 1
                                      , 1, 1, 1, 2
                                      , 2, 2, 2, 0
                                      , 0, 0, 0, 1
                                      ])

runTest5 = sampleN 21 $ expectedOutput5 $ bundle $ saturatingCounterWithReset 3 $ unbundle testInput5
runTest8 = sampleN 28 $ expectedOutput8 $ skipCounter 2 1 testInput8
