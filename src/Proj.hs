module Proj
  where

import qualified Data.List as List
import CLaSH.Prelude
import CLaSH.Prelude.Explicit

import Counter
import qualified Hardware.Uart as Uart
import Types
import DataFlow

generalFlow interDF = Uart.uart2df >~> (df $ (fifoDF d128 Nil) >=> interDF >=> (fifoDF d32 Nil)) >~> Uart.df2uart

(.++.) = liftA2 (++#)

-- A device connected via USB
topEntityUart (pll_is_locked, sw, debug_flag
              , uart_out, uart_out_stb, uart_in_ack) = (uart_in, uart_in_stb, uart_out_ack, ledScreen)
  where
    uart1 = bundle $ (generalFlow idDF) uart_out uart_out_stb uart_in_ack :: Signal (BitVector 8, Bit, Bit)
    (uart_in, uart_in_stb, uart_out_ack) = unbundle uart1
    ledScreen = 0 .++. lastUartSymbol
    lastUartSymbol = regEn 0 ((high==) `fmap` uart_out_stb) uart_out

{-# ANN topEntity
  (defTop
    { t_name     = "Proj"
    , t_inputs   = ["pll_is_locked", "sw", "debug_flag"
                   , "uart_data_out", "uart_data_out_stb", "uart_data_in_ack"]
    , t_outputs  = ["uart_data_in", "uart_data_in_stb", "uart_data_out_ack"
                   , "led_screen"]
    }) #-}
topEntity :: ( Signal Bit
             , Signal (BitVector 2)
             , Signal Bit
             , Signal (BitVector 8)
             , Signal Bit
             , Signal Bit)
          -> ( Signal (BitVector 8)
             , Signal Bit
             , Signal Bit
             , Signal (BitVector 32))
topEntity = topEntityUart
