-- File: LedScreen.vhd
-- Author: Bogdan Penkovsky
-- Description: An eight-segment LED screen controller.
-- Tested on Digilent Nexys4 board.


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity LedScreen is
    port (
        x: in std_logic_vector(31 downto 0);
        clk: in std_logic;
        reset: in std_logic;
        seg7_seg_O: out std_logic_vector(6 downto 0);
        seg7_dp_O: out std_logic;
        seg7_an_O: out std_logic_vector(7 downto 0)
    );
end entity LedScreen;

architecture MyArch of LedScreen is

constant SCREEN_DIGITS: integer := 8;

signal digit: std_logic_vector(3 downto 0);
signal clkdiv: unsigned(19 downto 0);
signal s: unsigned(2 downto 0);

begin

s <= clkdiv(20-1 downto 17);
seg7_dp_O <= '1';

LEDSCREEN_SELECT_DIGIT: process (s) is
begin
    for i in 0 to SCREEN_DIGITS-1 loop
        if (signed(resize(s, 4)) = i) then
            seg7_an_O(i) <= '0';
        else
            seg7_an_O(i) <= '1';
        end if;
    end loop;
end process LEDSCREEN_SELECT_DIGIT;

LEDSCREEN_MUX: process (x, s) is
begin
    case s is
        when "000" =>
            digit <= x(4-1 downto 0);
        when "001" =>
            digit <= x(8-1 downto 4);
        when "010" =>
            digit <= x(12-1 downto 8);
        when "011" =>
            digit <= x(16-1 downto 12);
        when "100" =>
            digit <= x(20-1 downto 16);
        when "101" =>
            digit <= x(24-1 downto 20);
        when "110" =>
            digit <= x(28-1 downto 24);
        when "111" =>
            digit <= x(32-1 downto 28);
        when others =>
            digit <= X"0";
    end case;
end process LEDSCREEN_MUX;

-- 7-segment decoder
--
LEDSCREEN_SINGLE_DIGIT_LOGIC: process (digit) is
begin
    case to_integer(unsigned(digit)) is
        when 0 => seg7_seg_O <= "1000000";
        when 1 => seg7_seg_O <= "1111001";
        when 2 => seg7_seg_O <= "0100100";
        when 3 => seg7_seg_O <= "0110000";
        when 4 => seg7_seg_O <= "0011001";
        when 5 => seg7_seg_O <= "0010010";
        when 6 => seg7_seg_O <= "0000010";
        when 7 => seg7_seg_O <= "1011000";
        when 8 => seg7_seg_O <= "0000000";
        when 9 => seg7_seg_O <= "0010000";
        when 10 => seg7_seg_O <= "0001000";
        when 11 => seg7_seg_O <= "0000011";
        when 12 => seg7_seg_O <= "1000110";
        when 13 => seg7_seg_O <= "0100001";
        when 14 => seg7_seg_O <= "0000110";
        when others => seg7_seg_O <= "0001110";
    end case;
end process LEDSCREEN_SINGLE_DIGIT_LOGIC;

LEDSCREEN_DIVIDE_CLOCK: process (clk, reset) is
begin
    if (reset = '1') then
        clkdiv <= to_unsigned(0, 20);
    elsif rising_edge(clk) then
        clkdiv <= (clkdiv + 1);
    end if;
end process LEDSCREEN_DIVIDE_CLOCK;

end architecture MyArch;
