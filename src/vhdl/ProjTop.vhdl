-- A wrapper
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- PLL simulation
library UNISIM;
use UNISIM.vcomponents.all;

entity ProjTop is
    port (
        clk100: in std_logic;
        reset: in std_logic;

        -- Switches
        sw : in std_logic_vector(1 downto 0);
        debug_flag : in std_logic_vector(0 downto 0);

        -- UART
        USB_RS232_RXD : in std_logic;
        USB_RS232_TXD : out std_logic;

        -- 7 segment display
        seg : out std_logic_vector(6 downto 0);
        dp : out std_logic;
        an : out std_logic_vector(7 downto 0)
    );
end ProjTop;

architecture Wrapper of ProjTop is

-- UART constants
constant BAUD_RATE : positive := 230400;
constant CLOCK_FREQUENCY : positive := 50000000;

component Proj is
  port(pll_is_locked     : in std_logic_vector(0 downto 0);
       sw                : in std_logic_vector(1 downto 0);
       debug_flag        : in std_logic_vector(0 downto 0);
       uart_data_out     : in std_logic_vector(7 downto 0);
       uart_data_out_stb : in std_logic_vector(0 downto 0);
       uart_data_in_ack  : in std_logic_vector(0 downto 0);
       -- clock
       system1000        : in std_logic;
       -- asynchronous reset: active low
       system1000_rstn   : in std_logic;
       uart_data_in      : out std_logic_vector(7 downto 0);
       uart_data_in_stb  : out std_logic_vector(0 downto 0);
       uart_data_out_ack : out std_logic_vector(0 downto 0);
       led_screen        : out std_logic_vector(31 downto 0));
end component;

component UART is
    generic (
            BAUD_RATE           : positive;
            CLOCK_FREQUENCY     : positive
        );
    port (  -- General
            CLOCK               :   in      std_logic;
            RESET               :   in      std_logic;
            DATA_STREAM_IN      :   in      std_logic_vector(7 downto 0);
            DATA_STREAM_IN_STB  :   in      std_logic;
            DATA_STREAM_IN_ACK  :   out     std_logic;
            DATA_STREAM_OUT     :   out     std_logic_vector(7 downto 0);
            DATA_STREAM_OUT_STB :   out     std_logic;
            DATA_STREAM_OUT_ACK :   in      std_logic;
            TX                  :   out     std_logic;
            RX                  :   in      std_logic
            );
end component UART;

component LedScreen is
    port (
        x: in std_logic_vector(31 downto 0);
        clk: in std_logic;
        reset: in std_logic;
        seg7_seg_O: out std_logic_vector(6 downto 0);
        seg7_dp_O: out std_logic;
        seg7_an_O: out std_logic_vector(7 downto 0)
    );
end component LedScreen;

signal feedback_clk : std_logic;
signal pll_is_locked : std_logic_vector(0 downto 0);
signal system1000 : std_logic;
-- Uart
signal uart_data_in : std_logic_vector(7 downto 0);
signal uart_data_out : std_logic_vector(7 downto 0);
signal uart_data_in_stb : std_logic_vector(0 downto 0);
signal uart_data_in_ack : std_logic_vector(0 downto 0);
signal uart_data_out_stb : std_logic_vector(0 downto 0);
signal uart_data_out_ack : std_logic_vector(0 downto 0);
-- Deglitch
signal tx, rx, rx_sync, reset_deglitched, not_reset_deglitched, reset_sync : std_logic;
-- To be displayed on 8 7-segment displays
signal led_screen : std_logic_vector(31 downto 0);

begin
    PROJ1 : Proj
    port map
    (
        pll_is_locked => pll_is_locked,
        sw => sw,
        debug_flag => debug_flag,
        uart_data_out => uart_data_out,
        uart_data_out_stb => uart_data_out_stb,
        uart_data_in_ack => uart_data_in_ack,

        system1000 => system1000,
        system1000_rstn => not_reset_deglitched,  -- reset low in CLaSH

        uart_data_in => uart_data_in,
        uart_data_in_stb => uart_data_in_stb,
        uart_data_out_ack => uart_data_out_ack,
        led_screen => led_screen
    );

    -- UART controller setup
    UART_inst1 : UART
    generic map (
                    BAUD_RATE => BAUD_RATE,
                    CLOCK_FREQUENCY => CLOCK_FREQUENCY
                )
    port map
    (
        CLOCK => system1000,
        RESET => reset_deglitched,
        DATA_STREAM_IN => uart_data_in,
        DATA_STREAM_IN_STB => uart_data_in_stb(0),
        DATA_STREAM_IN_ACK => uart_data_in_ack(0),
        DATA_STREAM_OUT => uart_data_out,
        DATA_STREAM_OUT_STB => uart_data_out_stb(0),
        DATA_STREAM_OUT_ACK => uart_data_out_ack(0),
        TX => tx,
        RX => rx
    );

    LEDSCREEN_inst1 : LedScreen
    port map
    (
        x => led_screen,
        clk => system1000,
        reset => reset_deglitched,
        seg7_seg_O => seg,
        seg7_dp_O => dp,
        seg7_an_O => an
    );

    DEGLITCH : process (system1000)
    begin
        if rising_edge(system1000) then
            rx_sync <= USB_RS232_RXD;
            rx <= rx_sync;
            reset_sync <= reset;
            reset_deglitched <= reset_sync;
            not_reset_deglitched <= not reset_sync;
            USB_RS232_TXD <= tx;
        end if;
    end process;

    -- PLLE2_BASE: Base Phase Locked Loop (PLL)
    --             Artix-7
    -- Xilinx HDL Language Template, version 2014.4

    PLLE2_BASE_inst : PLLE2_BASE
    generic map (
        BANDWIDTH => "OPTIMIZED",  -- OPTIMIZED, HIGH, LOW
        CLKFBOUT_MULT => 9,        -- Multiply value for all CLKOUT, (2-64)
        CLKFBOUT_PHASE => 0.0,     -- Phase offset in degrees of CLKFB, (-360.000-360.000).
        CLKIN1_PERIOD => 10.0,      -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
        -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
        CLKOUT0_DIVIDE => 18,  -- 50 Mhz
        CLKOUT1_DIVIDE => 1,  -- 36 MHz
        CLKOUT2_DIVIDE => 1,  -- 20 MHz
        CLKOUT3_DIVIDE => 1,
        CLKOUT4_DIVIDE => 1,
        CLKOUT5_DIVIDE => 1,
        -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for each CLKOUT (0.001-0.999).
        CLKOUT0_DUTY_CYCLE => 0.5,
        CLKOUT1_DUTY_CYCLE => 0.5,
        CLKOUT2_DUTY_CYCLE => 0.5,
        CLKOUT3_DUTY_CYCLE => 0.5,
        CLKOUT4_DUTY_CYCLE => 0.5,
        CLKOUT5_DUTY_CYCLE => 0.5,
        -- CLKOUT0_PHASE - CLKOUT5_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
        CLKOUT0_PHASE => 0.0,
        CLKOUT1_PHASE => 0.0,
        CLKOUT2_PHASE => 0.0,
        CLKOUT3_PHASE => 0.0,
        CLKOUT4_PHASE => 0.0,
        CLKOUT5_PHASE => 0.0,
        DIVCLK_DIVIDE => 1,        -- Master division value, (1-56)
        REF_JITTER1 => 0.0,        -- Reference input jitter in UI, (0.000-0.999).
        STARTUP_WAIT => "FALSE"    -- Delay DONE until PLL Locks, ("TRUE"/"FALSE")
    )
    port map (
        -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
        CLKOUT0 => system1000,   -- 1-bit output: CLKOUT0
        CLKOUT1 => OPEN,   -- 1-bit output: CLKOUT1
        CLKOUT2 => OPEN,   -- 1-bit output: CLKOUT2
        CLKOUT3 => OPEN,   -- 1-bit output: CLKOUT3
        CLKOUT4 => OPEN,   -- 1-bit output: CLKOUT4
        CLKOUT5 => OPEN,   -- 1-bit output: CLKOUT5
        -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
        CLKFBOUT => feedback_clk, -- 1-bit output: Feedback clock
        LOCKED => pll_is_locked(0),     -- 1-bit output: LOCK
        CLKIN1 => clk100,     -- 1-bit input: Input clock
        -- Control Ports: 1-bit (each) input: PLL control ports
        PWRDWN => '0',     -- 1-bit input: Power-down
        RST => reset,           -- 1-bit input: Reset
        -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
        CLKFBIN => feedback_clk    -- 1-bit input: Feedback clock
    );

    -- End of PLLE2_BASE_inst instantiation

end Wrapper;

-- vim: tw=80 expandtab shiftwidth=4 tabstop=4 smarttab
