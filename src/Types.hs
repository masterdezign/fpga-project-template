module Types
  where

import CLaSH.Prelude

-- 16 bit
type HistWord = Unsigned 16
