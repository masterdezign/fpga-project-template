SETTINGS=~/Xilinx/Vivado/2014.4/settings64.sh
VIVADO_CMD=~/Xilinx/Vivado/2014.4/bin/vivado
RUN_DIR=run
SRC_DIR=src
PROJ=Proj

.PHONY:
setup:
	source ${SETTINGS}

.PHONY: clean
clean: clean0
	cd ${SRC_DIR} && make clean PROJ=${PROJ}

.PHONY: clean0
clean0:
	rm -rf ${RUN_DIR}/out/

.PHONY:
console: setup
	cd ${RUN_DIR} && ${VIVADO_CMD} -mode tcl -source 01_setup.tcl

.PHONY:
build: setup clean
	cd ${SRC_DIR} && make build PROJ=${PROJ}
	cd ${RUN_DIR} && time ${VIVADO_CMD} -mode batch -source batch.tcl

.PHONY:
build0: setup clean0
	cd ${RUN_DIR} && time ${VIVADO_CMD} -mode batch -source batch.tcl

.PHONY:
test:
	cd ${SRC_DIR}; \
		make test

.PHONY:
minicom:
	sudo minicom -D /dev/ttyUSB1 -b 230400 -8 -o
